# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.

from trytond.pool import Pool
from . import commission
from . import invoice
from . import sale
from . import agent
from . import party


def register():
    Pool.register(
        commission.Plan,
        commission.Commission,
        sale.Sale,
        invoice.Invoice,
        commission.CreateInvoiceAsk,
        agent.AgentCommissionSalesStart,
        party.Party,
        module='commission_global', type_='model')
    Pool.register(
        commission.CreateInvoice,
        commission.CreateInvoiceDirect,
        agent.AgentCommissionSales,
        module='commission_global', type_='wizard')
    Pool.register(
        commission.CreateInvoice,
        commission.CreateInvoiceDirect,
        agent.AgentCommissionSalesReport,
        module='commission_global', type_='report')
